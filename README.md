# Simple proof of concept Comments / Notes app

# Made using

- [rangy](https://github.com/timdown/rangy) 
- jQuery
- AngularJS
- moment.js
- Twitter Bootstrap
- sinatra
- ruby

# Screenshots
![1](https://i.imgur.com/ppB696M.png)

![2](https://i.imgur.com/CfYMymV.png)

![3](https://i.imgur.com/JdPeQbU.png)

![4](https://i.imgur.com/OVbN12o.png)

# Requirements
- ruby 2.x
- bundler

# Run

```
git clone git@bitbucket.org:aladac_work/comments.git
cd comments
bundle install
foreman start
```